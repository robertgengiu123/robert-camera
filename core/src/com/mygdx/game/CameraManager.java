package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;

public class CameraManager {
    private PerspectiveCamera camera;

    public CameraManager(float fieldOfViewY) {
        camera = new PerspectiveCamera(fieldOfViewY, Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
    }

    public void set() {
        camera.position.set(0f, 300f, 400f);
        camera.lookAt(0f, 150f, 0f);

        camera.near = 0.1f;
        camera.far = 500.0f;
    }

    public PerspectiveCamera getCamera() {
        return camera;
    }

    public void updateCamera() {
        camera.update();
    }
}
