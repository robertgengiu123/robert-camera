package com.mygdx.game;

import com.badlogic.gdx.Game;

import java.util.LinkedList;


public class MyGdxGame extends Game {

    private static final int MAIN = 0;
    private static final int MENU = 1;
    private static final int LOGIN = 2;
    private static final int LOADING = 3;

    public MainScreen mainScreen;
    public LoginScreen loginScreen;
    public MenuScreen menuScreen;
    public LoadingScreen loadingScreen;
    public static boolean closed = false;

    public LinkedList<String> userList;
    public static String current_user;
    public static NetworkWorker worker;


    @Override
    public void create() {
        userList = new LinkedList<>();

        userList.add("User1");
        userList.add("User2");
        userList.add("User3");
        userList.add("User4");
        userList.add("User5");

        current_user = userList.get(0);

        mainScreen = new MainScreen(this);

        loadingScreen = new LoadingScreen(this);

        worker = new NetworkWorker();
        worker.start();

        changeScreen(LOADING);

        mainScreen.load();


    }

    @Override
    public void dispose() {
        closed = true;
    }

    @Override
    public void render() {
        super.render();
    }

    public void changeScreen(int screen){
        switch(screen) {
            case MENU:
                if (menuScreen == null) menuScreen = new MenuScreen();
                this.setScreen(menuScreen);
                break;
            case LOADING:
                if (loadingScreen == null) loadingScreen = new LoadingScreen(this);
                this.setScreen(loadingScreen);
                break;
            case MAIN:
                if (mainScreen == null) mainScreen = new MainScreen(this);
                this.setScreen(mainScreen);
                break;
            case LOGIN:
                if (loginScreen == null) loginScreen = new LoginScreen(this);
                this.setScreen(loginScreen);
                break;
        }
    }

    public static void receiveJoints(double[] joints) {
        JointsBuffer.receiveUpdate(joints);

    }



}
