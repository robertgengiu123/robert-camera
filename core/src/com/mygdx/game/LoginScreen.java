package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.awt.Font;
import java.time.format.TextStyle;
import java.util.LinkedList;

import sun.security.util.Password;

import static java.awt.Font.SANS_SERIF;

public class LoginScreen implements Screen {
    private static final int MAIN = 0;
    private static final int MENU = 1;
    private static final int LOGIN = 2;
    private static final int LOADING = 3;

    private MyGdxGame parent;
    private boolean accesGranted = false;
    private Stage stage;
    private TextButton btnLogin;
    private TextField txfUsername;
    private TextField txfPassword;
    private LinkedList<ImageButton> users;
    private LinkedList<TextField> usernames;
    private static int index;


    LoginScreen(MyGdxGame game) {
        parent = game;

        stage = new Stage();

        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2.0f);
        skin.getFont("password-font").getData().setScale(2.0f);

//        btnLogin = new TextButton("Confirma", skin);
//        btnLogin.setPosition(Gdx.graphics.getWidth() / 2 - 250,
//                Gdx.graphics.getHeight() / 2 + 300);
//        btnLogin.setSize(500, 125);
//        //btnLogin.getLabel().setFontScale(6.5f);
//        btnLogin.getLabel().setColor(Color.WHITE);
//
//        btnLogin.addListener(new ClickListener() {
//            @Override
//            public void clicked(InputEvent e, float x, float y) {
//                processLogin();
//            }
//
//        });
//
//        txfUsername = new TextField("", skin);
//        txfUsername.setSize(400, 100);
//        txfUsername.setPosition(Gdx.graphics.getWidth() / 2 - 200,
//                Gdx.graphics.getHeight() / 2 + 150 );
//
//        txfPassword = new TextField("", skin, "password");
//        txfPassword.setSize(400, 100);
//        txfPassword.setPosition(Gdx.graphics.getWidth() / 2 - 200,
//                Gdx.graphics.getHeight() / 2 );
//
//
//        stage.addActor(btnLogin);
//        stage.addActor(txfUsername);
//        stage.addActor(txfPassword);

        users = new LinkedList<>();
        usernames = new LinkedList<>();

        Texture texture = new Texture(Gdx.files.internal("buttons/user.jpg"));

        for (int i = 0; i < parent.userList.size(); i++) {
            ImageButton image = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture)));
            image.setSize(200, 200);
            image.setPosition(Gdx.graphics.getWidth() / 5.00f * i + 100,
                    Gdx.graphics.getHeight() / 2.00f);

            index = i;
            image.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent e, float x, float y) {
                    parent.current_user = parent.userList.get(index);
                    parent.changeScreen(MAIN);
                }

            });
            users.add(image);

            txfUsername = new TextField(parent.userList.get(i), skin);
            txfUsername.setSize(200, 70);
            txfUsername.setPosition(Gdx.graphics.getWidth() / 5.00f * i + 100,
                    Gdx.graphics.getHeight() / 2.00f - 150);

            usernames.add(txfUsername);

            stage.addActor(users.get(i));
            stage.addActor(usernames.get(i));
        }


    }

    public void processLogin() {
        if (txfUsername.getText().equals("admin") && txfPassword.getText().equals("admin")) {
            btnLogin.setText("Works!");
            parent.changeScreen(MAIN);
        }
        else {
            btnLogin.setText("Wrong username or password");
        }
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {

        if (accesGranted == true) {
            parent.changeScreen(MENU);
        }

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
