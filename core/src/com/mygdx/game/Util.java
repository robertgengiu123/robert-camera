package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

public class Util {
    private static final int JOINT_HEAD = 0;
    private static final int JOINT_NECK = 2;
    private static final int JOINT_LEFT_COLAR = 4;
    private static final int JOINT_TORSO = 6;
    private static final int JOINT_LEFT_SHOULDER = 8;
    private static final int JOINT_RIGHT_SHOULDER = 10;
    private static final int JOINT_WAIST = 12;
    private static final int JOINT_LEFT_HIP = 14;
    private static final int JOINT_RIGHT_HIP = 16;
    private static final int JOINT_LEFT_ELBOW = 18;
    private static final int JOINT_LEFT_WRIST = 20;
    private static final int JOINT_LEFT_HAND = 22;
    private static final int JOINT_RIGHT_ELBOW = 24;
    private static final int JOINT_RIGHT_WRIST = 26;
    private static final int JOINT_RIGHT_HAND = 28;
    private static final int JOINT_RIGHT_KNEE = 30;
    private static final int JOINT_LEFT_KNEE = 32;
    private static final int JOINT_RIGHT_ANKLE = 34;
    private static final int JOINT_LEFT_ANKLE = 36;



    private static ShapeRenderer debugRenderer = new ShapeRenderer();

    public static void DrawDebugLine(Vector2 start, Vector2 end, int lineWidth, Color color, Matrix4 projectionMatrix)
    {
        Gdx.gl.glLineWidth(lineWidth);
        debugRenderer.setProjectionMatrix(projectionMatrix);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);
        debugRenderer.setColor(color);
        debugRenderer.line(start, end);
        debugRenderer.end();
        Gdx.gl.glLineWidth(1);
    }

    public static void DrawDebugLine(Vector2 start, Vector2 end, Matrix4 projectionMatrix)
    {
        Gdx.gl.glLineWidth(2);
        debugRenderer.setProjectionMatrix(projectionMatrix);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);
        debugRenderer.setColor(Color.WHITE);
        debugRenderer.line(start, end);
        debugRenderer.end();
        Gdx.gl.glLineWidth(1);
    }

    public static void DrawSkeleton(float[] joints, Matrix4 projectionMatrix) {
            Color color = Color.GREEN;

            DrawDebugLine(new Vector2(joints[JOINT_HEAD], joints[JOINT_HEAD + 1]),
                    new Vector2(joints[JOINT_NECK], joints[JOINT_NECK + 1]),
                    10, color,
                    projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_NECK], joints[JOINT_NECK + 1]),
                    new Vector2(joints[JOINT_LEFT_COLAR], joints[JOINT_LEFT_COLAR + 1]),
                    10, color,
                    projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_LEFT_COLAR], joints[JOINT_LEFT_COLAR + 1]),
                new Vector2(joints[JOINT_TORSO], joints[JOINT_TORSO + 1]),
                    10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_LEFT_COLAR], joints[JOINT_LEFT_COLAR + 1]),
                    new Vector2(joints[JOINT_LEFT_SHOULDER], joints[JOINT_LEFT_SHOULDER + 1]),
                    10, color,
                    projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_LEFT_COLAR], joints[JOINT_LEFT_COLAR + 1]),
                    new Vector2(joints[JOINT_RIGHT_SHOULDER], joints[JOINT_RIGHT_SHOULDER + 1]),
                    10, color,
                    projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_WAIST], joints[JOINT_WAIST + 1]),
                new Vector2(joints[JOINT_LEFT_HIP], joints[JOINT_LEFT_HIP + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_WAIST], joints[JOINT_WAIST+ 1]),
                new Vector2(joints[JOINT_RIGHT_HIP], joints[JOINT_RIGHT_HIP + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_TORSO], joints[JOINT_TORSO+ 1]),
                new Vector2(joints[JOINT_WAIST], joints[JOINT_WAIST + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_LEFT_SHOULDER], joints[JOINT_LEFT_SHOULDER + 1]),
                new Vector2(joints[JOINT_LEFT_ELBOW], joints[JOINT_LEFT_ELBOW + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_LEFT_ELBOW], joints[JOINT_LEFT_ELBOW+ 1]),
                new Vector2(joints[JOINT_LEFT_WRIST], joints[JOINT_LEFT_WRIST + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_LEFT_WRIST], joints[JOINT_LEFT_WRIST+ 1]),
                new Vector2(joints[JOINT_LEFT_HAND], joints[JOINT_LEFT_HAND + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_RIGHT_SHOULDER], joints[JOINT_RIGHT_SHOULDER+ 1]),
                new Vector2(joints[JOINT_RIGHT_ELBOW], joints[JOINT_RIGHT_ELBOW + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_RIGHT_ELBOW], joints[JOINT_RIGHT_ELBOW+ 1]),
                new Vector2(joints[JOINT_RIGHT_WRIST], joints[JOINT_RIGHT_WRIST + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_RIGHT_WRIST], joints[JOINT_RIGHT_WRIST + 1]),
                new Vector2(joints[JOINT_RIGHT_HAND], joints[JOINT_RIGHT_HAND + 1]),
                10, color,
                projectionMatrix);

            DrawDebugLine(new Vector2(joints[JOINT_RIGHT_HIP], joints[JOINT_RIGHT_HIP + 1]),
                new Vector2(joints[JOINT_RIGHT_KNEE], joints[JOINT_RIGHT_KNEE + 1]),
                10, color,
                projectionMatrix);


            DrawDebugLine(new Vector2(joints[JOINT_LEFT_HIP], joints[JOINT_LEFT_HIP + 1]),
                new Vector2(joints[JOINT_LEFT_KNEE], joints[JOINT_LEFT_KNEE + 1]),
                10, color,
                projectionMatrix);


            DrawDebugLine(new Vector2(joints[JOINT_RIGHT_KNEE], joints[JOINT_RIGHT_KNEE + 1]),
                new Vector2(joints[JOINT_RIGHT_ANKLE], joints[JOINT_RIGHT_ANKLE + 1]),
                10, color,
                projectionMatrix);


            DrawDebugLine(new Vector2(joints[JOINT_LEFT_KNEE], joints[JOINT_LEFT_KNEE + 1]),
                new Vector2(joints[JOINT_LEFT_ANKLE], joints[JOINT_LEFT_ANKLE + 1]),
                10, color,
                projectionMatrix);



    }
}
