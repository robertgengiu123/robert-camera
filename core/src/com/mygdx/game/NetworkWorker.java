package com.mygdx.game;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import sun.rmi.runtime.Log;

public class NetworkWorker extends Thread{
    @Override
    public void run() {
        System.out.println("started");
        HttpClient httpClient = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000);
        while (true) {
            while (!JointsBuffer.queue.isEmpty()) {
                JSONObject object = null;
                try {
                    object = JointsBuffer.queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (object != null) {
                    try {
                        HttpPost request = new HttpPost(
                                "http://141.85.223.52/ioserver/api/camera-fitness-data");
                        StringEntity params = new StringEntity(object.toString());
                        request.addHeader("content-type", "application/json");
                        request.setEntity(params);
                        HttpResponse response = httpClient.execute(request);
                        response.getEntity().consumeContent();
                        StatusLine statusLine = response.getStatusLine();
                        int statusCode = statusLine.getStatusCode();

                        if (statusCode == 201) {
                            InputStream in = response.getEntity().getContent();
                            System.out.println("RESPONSE: OK");
                            try {
                                StringBuilder sb = new StringBuilder();
                                BufferedReader bufferedReader  = new BufferedReader(
                                        new InputStreamReader(in), 8);
                                String line;
                                while( ( line = bufferedReader.readLine() ) != null ) {
                                    sb.append( line );
                                }
                                bufferedReader.close();
                                in.close();
                                System.out.println("Content: " + sb.toString());
                            } catch (Exception ex) {
                                System.out.println( "statusCode " + ex.getMessage() + "" );
                            }

                        } else {
                            System.out.println("NO RESPONSE " + statusCode);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        System.out.println("Error " + "Cannot Estabilish Connection");
                    }
                }

            }
        }
    }
}
