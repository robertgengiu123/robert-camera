package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class SoundManager {
    private static Music backgroundMusic;

    public SoundManager(String path) {
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal(path));
        backgroundMusic.setLooping(true);
        backgroundMusic.setVolume(1.0f);
    }

    public void playMusic() {
        backgroundMusic.play();
    }

    public void pauseMusic() {
        backgroundMusic.pause();
    }

    public void dispose() {
        backgroundMusic.dispose();
    }

    public void setVolume(float volume) {
        if (volume - 1.0f <= 0.0f) {
            backgroundMusic.setVolume(volume);
        }
    }
}
