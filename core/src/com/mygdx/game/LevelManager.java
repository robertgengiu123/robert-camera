package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;


public class LevelManager {
    private Array<ModelInstance> instances;
    private ModelLoader loader;
    private Model couch;
    private Model chair;
    private Model carpet;
    private Model cabinet;
    private Model curio;

    public LevelManager() {
        instances = new Array<ModelInstance>();
        loader = new ObjLoader();
    }

    public void create() {
//        couch = loader.loadModel(Gdx.files.internal("Sofa/atelier-pfister-riom-sofa.obj"));
//        ModelInstance couchInstance = new ModelInstance(couch);
//        couchInstance.transform.setToTranslation(-200.0f, 70.0f, -60f);
//        couchInstance.transform.scale(19.0f, 19.0f, 19.0f);
//        instances.add(couchInstance);
//
//        chair = loader.loadModel(Gdx.files.internal("Chair/chair.obj"));
//        ModelInstance chairInstance = new ModelInstance(chair);
//        chairInstance.transform.setToTranslation(250.0f, 60.0f, 155f);
//        chairInstance.transform.scale(2.5f, 2.5f, 2.5f);
//        instances.add(chairInstance);
//
//        carpet = loader.loadModel(Gdx.files.internal("Carpet/carpet.obj"));
//        ModelInstance carpetInstance = new ModelInstance(carpet);
//        carpetInstance.transform.setToTranslation(0f, 30f, 100f);
//        carpetInstance.transform.scale(3400, 3400, 3400);
//        //carpetInstance.transform.rotate(Vector3.Y, 90);
//        instances.add(carpetInstance);

        cabinet = loader.loadModel(Gdx.files.internal("Furniture/Cabinet/hsrcbntccfr.blend.obj"));
        ModelInstance interiorInstance = new ModelInstance(cabinet);
        interiorInstance.transform.setToTranslation(-340.0f, 60.0f, 10f);
        interiorInstance.transform.scale(20.0f, 20.0f, 20.0f);
        instances.add(interiorInstance);

    }

    public void dispose() {
        couch.dispose();
        chair.dispose();
        carpet.dispose();
    }

    public Array<ModelInstance> getInstances() {
        return instances;
    }
}
