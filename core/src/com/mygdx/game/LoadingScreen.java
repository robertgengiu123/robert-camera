package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LoadingScreen implements Screen {

    private static final int MAIN = 0;
    private static final int MENU = 1;
    private static final int LOGIN = 2;
    private static final int LOADING = 3;

    MyGdxGame parent;

    Texture loadingBackground;
    SpriteBatch spriteBatch;

    LoadingScreen(MyGdxGame game) {
        parent = game;

        loadingBackground = new Texture(Gdx.files.internal("LoadingImage/loading.jpeg"));
        spriteBatch = new SpriteBatch();

    }

    @Override
    public void show() {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        spriteBatch.begin();
        spriteBatch.draw(loadingBackground, 0, 0,
                Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        spriteBatch.end();


    }

    @Override
    public void render(float delta) {

        if (parent.mainScreen.loaded == true) {
            parent.changeScreen(LOGIN);
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        loadingBackground.dispose();
    }


}
