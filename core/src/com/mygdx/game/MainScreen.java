package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import sun.rmi.runtime.Log;

public class MainScreen implements Screen {
    private ModelBatch modelBatch;
    private Environment environment;
    SoundManager soundManager;
    CameraManager cameraManager;
    LevelManager levelManager;
    CharacterManager characterManager;
    MyGdxGame parent;
    public boolean loaded = false;
    private Stage stage;
    Skin skin;
    private TextButton changeButtonUp;
    private TextButton changeButtonDown;

    private TextButton start;


    private TextButton logout;
    private Texture rightImage;
    private Texture leftImage;
    private ImageButton rotateLeft;
    private ImageButton rotateRight;
    private OrthographicCamera orthographicCamera;
    private static final int LOGIN = 2;
    public static int id_exercitiu = 0;
    public static boolean ok = false;

    public MainScreen(MyGdxGame game) {
        parent = game;
        stage = new Stage();
        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
    }

    public void load() {
        cameraManager = new CameraManager(75.0f);
        cameraManager.set();

        orthographicCamera = new OrthographicCamera(400, Gdx.graphics.getHeight());

        orthographicCamera.position.set(orthographicCamera.viewportWidth / 2.0f,
                -100.0f, 0.0f);

        orthographicCamera.update();
        //orthographicCamera.lookAt(0, 100, 180);
        //orthographicCamera.near = 0.1f;
        //orthographicCamera.far = 500.0f;

        modelBatch = new ModelBatch();

        levelManager = new LevelManager();
        levelManager.create();

        characterManager = new CharacterManager();
        characterManager.create();


        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight,
                0.8f, 0.8f, 0.8f, 1.0f));


        soundManager = new SoundManager("hiphop.mp3");
        //soundManager.playMusic();
        loaded = true;

        skin.getFont("default-font").getData().setScale(2.0f);

        changeButtonUp = new TextButton("Urmatorul Exercitiu", skin);
        changeButtonUp.setPosition(Gdx.graphics.getWidth() - 320, 20);
        changeButtonUp.setSize(300, 75);

        changeButtonDown = new TextButton("Exercitiul Anterior", skin);
        changeButtonDown.setPosition(20, 20);
        changeButtonDown.setSize(300, 75);

        start = new TextButton("Start", skin);
        start.setPosition(Gdx.graphics.getWidth() - 320, 120);
        start.setSize(300, 75);



        logout = new TextButton("Logout", skin);
        logout.setPosition(Gdx.graphics.getWidth() - 320, Gdx.graphics.getHeight() - 200);
        logout.setSize(300, 75);

        changeButtonUp.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                characterManager.changeAnimation(characterManager.getCurrentAnimation() + 1);
                id_exercitiu = characterManager.getCurrentAnimation();
            }
        });

        changeButtonDown.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                characterManager.changeAnimation(characterManager.getCurrentAnimation() - 1);
                id_exercitiu = characterManager.getCurrentAnimation();
            }
        });


        logout.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                parent.changeScreen(LOGIN);
            }
        });


        start.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                if (MainScreen.ok == false) {
                    MainScreen.ok = true;
                    start.setText("Stop");
                } else {
                    MainScreen.ok = false;
                    start.setText("Start");
                }
            }
        });


        stage.addActor(start);
        stage.addActor(changeButtonUp);
        stage.addActor(changeButtonDown);
        stage.addActor(logout);

        rightImage = new Texture(Gdx.files.internal("buttons/rotate_right.png"));
        leftImage = new Texture(Gdx.files.internal("buttons/rotate_left.png"));

        rotateLeft = new ImageButton(new TextureRegionDrawable(new TextureRegion(leftImage)));
        rotateRight = new ImageButton(new TextureRegionDrawable(new TextureRegion(rightImage)));

        rotateRight.setSize(75, 75);
        rotateLeft.setSize(75, 75);

        rotateRight.setPosition(Gdx.graphics.getWidth() / 2 - 150, 20);
        rotateLeft.setPosition(Gdx.graphics.getWidth() / 2 + 75, 20);

        rotateRight.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                characterManager.rotateCharacter((float)(15.0f / Math.PI));
            }
        });

        rotateLeft.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                characterManager.rotateCharacter((float)(-15.0f / Math.PI));
            }
        });

        stage.addActor(rotateLeft);
        stage.addActor(rotateRight);


    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        Gdx.input.setOnscreenKeyboardVisible(false);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        orthographicCamera.update();

        Gdx.gl.glViewport(0, 0, 400, Gdx.graphics.getHeight());
        modelBatch.begin(orthographicCamera);
        //modelBatch.render(skeleton.getBones());
        modelBatch.end();
        Util.DrawSkeleton(JointsBuffer.getJoints(), orthographicCamera.combined);

        Util.DrawDebugLine(new Vector2(0, -Gdx.graphics.getHeight() / 2 + 3),
                new Vector2(400, -Gdx.graphics.getHeight() / 2 + 3),
                3, Color.RED, orthographicCamera.combined );
        Util.DrawDebugLine(new Vector2(0, 0), new Vector2(0, Gdx.graphics.getHeight()),
                3, Color.GREEN, orthographicCamera.combined);
        Util.DrawDebugLine(new Vector2(0, Gdx.graphics.getHeight() - 3),
                new Vector2(400, Gdx.graphics.getHeight() - 3),
                3, Color.BLUE, orthographicCamera.combined );
        Util.DrawDebugLine(new Vector2(400, -Gdx.graphics.getHeight() / 2 + 3), new Vector2(400,
                        Gdx.graphics.getHeight() - 3), 3,
                Color.BLACK, orthographicCamera.combined );

        cameraManager.updateCamera();
        characterManager.updateController(Gdx.graphics.getDeltaTime());
        Gdx.gl.glViewport(400, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        modelBatch.begin(cameraManager.getCamera());
        modelBatch.render(characterManager.getModelInstance(), environment);
        modelBatch.render(levelManager.getInstances(), environment);
        modelBatch.end();


        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.act(delta);
        stage.draw();





    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        modelBatch.dispose();
        characterManager.dispose();
        soundManager.dispose();
        levelManager.dispose();
    }
}
