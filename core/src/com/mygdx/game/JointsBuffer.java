package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;


public class JointsBuffer {
    private static double[] buffer = new double[38];
    private static float[] outputBuffer = new float[38];
    private static long delta = 0;
    private static long current_time = 0;
    private static long last_time = 0;
    private static long maxDelta = 5;
    private static LinkedList<String> jointsName;
    public static BlockingQueue<JSONObject> queue;
    private static int counter = 0;
    private static int maxCounter = 13;

    static {
        queue = new LinkedBlockingDeque<>();
        for (int i = 0; i < 38; i++) {
            buffer[i] = 0.0;
        }

        jointsName = new LinkedList<>();

        jointsName.add("joint_head");           jointsName.add("joint_neck");
        jointsName.add("joint_left_collar");    jointsName.add("joint_torso");
        jointsName.add("joint_left_shoulder");  jointsName.add("joint_right_shoulder");
        jointsName.add("joint_waist");          jointsName.add("joint_left_hip");
        jointsName.add("joint_right_hip");      jointsName.add("joint_left_elbow");
        jointsName.add("joint_left_wrist");     jointsName.add("joint_left_hand");
        jointsName.add("joint_right_elbow");    jointsName.add("joint_right_wrist");
        jointsName.add("joint_right_hand");     jointsName.add("joint_right_knee");
        jointsName.add("joint_left_knee");      jointsName.add("joint_right_ankle");
        jointsName.add("joint_left_ankle");
    }

    public static void receiveUpdate(double[] joints) {
        buffer = joints;
        delta = 0;
        last_time = System.currentTimeMillis() / 1000;
        counter = (counter + 1) % maxCounter;

        // To do create JSON and send it

        if (MainScreen.id_exercitiu != 0 && MainScreen.ok == true && counter == 0) {
            JSONObject object = new JSONObject();
            JSONObject data = new JSONObject();
            JSONObject exercise = new JSONObject();
            JSONObject skeleton = new JSONObject();

            for (int i = 0; i < jointsName.size(); i++) {
                JSONObject joint = new JSONObject();
                joint.put("real_x", new Double(buffer[i * 4 + 38]));
                joint.put("real_y", new Double(buffer[i * 4 + 39]));
                joint.put("real_z", new Double(buffer[i * 4 + 40]));
                joint.put("confidence", new Double(buffer[i * 4 + 41]));
                skeleton.put(jointsName.get(i), joint);
            }


            exercise.put("name", CharacterManager.animations.get(MainScreen.id_exercitiu));
            exercise.put("user", MyGdxGame.current_user);
            exercise.put("skeleton", skeleton);

            data.put("exercise", exercise);

            object.put("deviceUUID", "123");
            object.put("timestamp", new Long(System.currentTimeMillis()));
            object.put("data", "\"" + data + "\"");

            System.out.println(object);
            try {
                queue.put(object);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }



    }

    public static float[] getJoints() {

        current_time = System.currentTimeMillis() / 1000;
        delta = current_time - last_time;

        if (delta > maxDelta) {
            for (int i = 0; i < 38; i++) {
                outputBuffer[i] = 0;
            }

            return outputBuffer;

        }


        for (int i = 0; i < 38; i += 2) {
            outputBuffer[i] = (float)buffer[i] * 380;
            outputBuffer[i+1] = Gdx.graphics.getHeight() / 6
                     - (float)buffer[i+1] * 240;
        }

        return outputBuffer;
    }

}
