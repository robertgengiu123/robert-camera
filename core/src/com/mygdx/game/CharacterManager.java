package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.model.data.ModelData;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.TextureProvider;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.UBJsonReader;

public class CharacterManager {
    private Model model;
    private ModelData modelData;
    private Array<ModelInstance> modelInstances = new Array<>();
    ModelLoader<?> modelLoader;
    private AnimationController controller;

    public int getCurrentAnimation() {
        return currentAnimation;
    }

    private int currentAnimation;
    public static Array<String> animations = new Array<String>();


    public CharacterManager() {
        modelLoader = new G3dModelLoader(new UBJsonReader());
        currentAnimation = 0;
        animations.add("IdleAnimation", "AirSquat", "BicepsCurlAnimation", "KneeingAnimation");
        animations.add("KneeingMirrorAnimation", "FrontRaiseAnimation");
    }

    private void setAnimation(int id) {

        controller = new AnimationController(modelInstances.get(id));
        controller.paused = false;

        controller.setAnimation("mixamo.com", 5,
                new AnimationController.AnimationListener() {
                    @Override
                    public void onEnd(AnimationController.AnimationDesc animation) {
                        setAnimation(0);

                    }

                    @Override
                    public void onLoop(AnimationController.AnimationDesc animation) {
                        Gdx.app.log("INFO", "Animation Ended");
                    }
                });
        currentAnimation = id;
    }


    public void create() {
        for (int i = 0; i < animations.size; i++) {
            modelData = modelLoader.loadModelData(Gdx.files.internal(animations.get(i) +
                    "/model.g3db"));
            model = new Model(modelData, new TextureProvider.FileTextureProvider());
            modelInstances.add(new ModelInstance(model));
            modelInstances.get(i).transform.setToTranslation(-70f, 20f, 110f);
        }


        this.setAnimation(currentAnimation);

    }

    public void dispose() {
        model.dispose();
    }

    public void changeAnimation(int id) {
        if (currentAnimation == id || id >= animations.size || id < 1) {
            return;
        }

        controller.paused = true;
        setAnimation(id);
    }

    public ModelInstance getModelInstance() {
        return modelInstances.get(currentAnimation);
    }

    public void updateController(float deltaTime) {
        controller.update(deltaTime);
    }

    public void rotateCharacter(float angle) {
        for (int i = 0; i < animations.size; i++) {
            modelInstances.get(i).transform.rotateRad(Vector3.Y, angle);
        }
    }
}
