#include <jni.h>
#include <string>

////////////////
#include <nuitrack/Nuitrack.h>

#ifdef _WIN32
#include <Windows.h>
#endif

#ifndef ANDROID
#include <GL/gl.h>
#else
#include <GLES/gl.h>
#endif

#include <string>
#include <jni.h>

#include <EGL/egl.h>
#include <android_native_app_glue.h>
////////////////

#include <iomanip>
#include <iostream>
#include <android/log.h>

#include <unistd.h>
#include <atomic>

static JavaVM* g_JavaVM = NULL;
static std::atomic<bool> running(true);

extern "C" JNIEXPORT jstring
JNICALL
Java_com_mygdx_game_AndroidLauncher_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

using namespace tdv::nuitrack;

void sendToJava(const std::vector<Joint>& joints);

void onSkeletonUpdate(SkeletonData::Ptr userSkeletons)
{
    //__android_log_write(ANDROID_LOG_INFO, "Radu", "Got some skeleton data");
	auto skeletons = userSkeletons->getSkeletons();
	int c = 0;
	for (auto skeleton: skeletons)
	{
		//drawSkeleton(skeleton.joints);
		sendToJava(skeleton.joints);
        __android_log_write(ANDROID_LOG_INFO, "Radu", "Got some skeleton");
        break;
	}
	//__android_log_write(ANDROID_LOG_INFO, "Radu", "Finished getting some skeleton data");
}

// starts tracking (runs continuously)
extern "C" JNIEXPORT jstring JNICALL Java_com_mygdx_game_AndroidLauncher_startTracking(JNIEnv *env, jobject) {
    std::string hello = "OK";

    // Initialize Nuitrack
    try
    {
        Nuitrack::init("");
    }
    catch (const Exception& e)
    {
        __android_log_write(ANDROID_LOG_ERROR, "Radu", "Can not initialize Nuitrack");
        return env->NewStringUTF("EXIT_FAILURE");
    }

    __android_log_write(ANDROID_LOG_INFO, "Radu", "Initialized");

    tdv::nuitrack::SkeletonTracker::Ptr skeletonTracker = SkeletonTracker::create();

    skeletonTracker->connectOnUpdate(onSkeletonUpdate);

    try
    {
        Nuitrack::run();
    }
    catch (const Exception& e)
    {
        __android_log_write(ANDROID_LOG_ERROR, "Radu", "Can not start Nuitrack");
        return env->NewStringUTF("EXIT_FAILURE");
    }

    __android_log_write(ANDROID_LOG_INFO, "Radu", "Nuitrack running");
    running = true;

    int errorCode = EXIT_SUCCESS;
    while (running.load())
    {
        try
        {
            // Wait for new skeleton tracking data
            Nuitrack::waitUpdate(skeletonTracker);
        }
        catch (LicenseNotAcquiredException& e)
        {
            __android_log_write(ANDROID_LOG_ERROR, "Radu", "License not acquired");
            return env->NewStringUTF("License not acquired");
        }
        catch (const Exception& e)
        {
            __android_log_write(ANDROID_LOG_ERROR, "Radu", "Nuitrack update failed");
            return env->NewStringUTF("Nuitrack update failed");
        }
    }

    return env->NewStringUTF(hello.c_str());
}

// stops tracking (actually stops the entire Nuitrack framework)
extern "C" JNIEXPORT jstring JNICALL Java_com_mygdx_game_AndroidLauncher_stopTracking(JNIEnv *env, jobject) {

    std::string hello = "OK";

    // Release Nuitrack
    try
    {
        Nuitrack::release();
    }
    catch (const Exception& e)
    {
        __android_log_write(ANDROID_LOG_ERROR, "Radu", "Nuitrack release failed");
        return env->NewStringUTF("Nuitrack release failed");
    }

    __android_log_write(ANDROID_LOG_INFO, "Radu", "Nuitrack release OK");
    running = false;

    return env->NewStringUTF(hello.c_str());
}

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
    g_JavaVM = vm;
    return JNI_VERSION_1_6;
}

void sendToJava(const std::vector<Joint>& joints){
    if (g_JavaVM == NULL) {
        __android_log_write(ANDROID_LOG_INFO, "Radu", "Java VM null");
        return;
    }

    JNIEnv *env;
    g_JavaVM->GetEnv((void**)&env, JNI_VERSION_1_6);

    jdoubleArray jarr1 = env->NewDoubleArray(114);
    jdouble *myjoints = env->GetDoubleArrayElements(jarr1, 0);

    int count = 0;
    myjoints[count++] = joints[JOINT_HEAD].proj.x;
    myjoints[count++] = joints[JOINT_HEAD].proj.y;
    myjoints[count++] = joints[JOINT_NECK].proj.x;
    myjoints[count++] = joints[JOINT_NECK].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_COLLAR].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_COLLAR].proj.y;
    myjoints[count++] = joints[JOINT_TORSO].proj.x;
    myjoints[count++] = joints[JOINT_TORSO].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_SHOULDER].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_SHOULDER].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_SHOULDER].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_SHOULDER].proj.y;
    myjoints[count++] = joints[JOINT_WAIST].proj.x;
    myjoints[count++] = joints[JOINT_WAIST].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_HIP].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_HIP].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_HIP].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_HIP].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_ELBOW].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_ELBOW].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_WRIST].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_WRIST].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_HAND].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_HAND].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_ELBOW].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_ELBOW].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_WRIST].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_WRIST].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_HAND].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_HAND].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_KNEE].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_KNEE].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_KNEE].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_KNEE].proj.y;
    myjoints[count++] = joints[JOINT_RIGHT_ANKLE].proj.x;
    myjoints[count++] = joints[JOINT_RIGHT_ANKLE].proj.y;
    myjoints[count++] = joints[JOINT_LEFT_ANKLE].proj.x;
    myjoints[count++] = joints[JOINT_LEFT_ANKLE].proj.y;

    myjoints[count++] = joints[JOINT_HEAD].real.x;
    myjoints[count++] = joints[JOINT_HEAD].real.y;
    myjoints[count++] = joints[JOINT_HEAD].real.z;
    myjoints[count++] = joints[JOINT_HEAD].confidence;

    myjoints[count++] = joints[JOINT_NECK].real.x;
    myjoints[count++] = joints[JOINT_NECK].real.y;
    myjoints[count++] = joints[JOINT_NECK].real.z;
    myjoints[count++] = joints[JOINT_NECK].confidence;

    myjoints[count++] = joints[JOINT_LEFT_COLLAR].real.x;
    myjoints[count++] = joints[JOINT_LEFT_COLLAR].real.y;
    myjoints[count++] = joints[JOINT_LEFT_COLLAR].real.z;
    myjoints[count++] = joints[JOINT_LEFT_COLLAR].confidence;

    myjoints[count++] = joints[JOINT_TORSO].real.x;
    myjoints[count++] = joints[JOINT_TORSO].real.y;
    myjoints[count++] = joints[JOINT_TORSO].real.z;
    myjoints[count++] = joints[JOINT_TORSO].confidence;

    myjoints[count++] = joints[JOINT_LEFT_SHOULDER].real.x;
    myjoints[count++] = joints[JOINT_LEFT_SHOULDER].real.y;
    myjoints[count++] = joints[JOINT_LEFT_SHOULDER].real.z;
    myjoints[count++] = joints[JOINT_LEFT_SHOULDER].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_SHOULDER].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_SHOULDER].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_SHOULDER].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_SHOULDER].confidence;

    myjoints[count++] = joints[JOINT_WAIST].real.x;
    myjoints[count++] = joints[JOINT_WAIST].real.y;
    myjoints[count++] = joints[JOINT_WAIST].real.z;
    myjoints[count++] = joints[JOINT_WAIST].confidence;

    myjoints[count++] = joints[JOINT_LEFT_HIP].real.x;
    myjoints[count++] = joints[JOINT_LEFT_HIP].real.y;
    myjoints[count++] = joints[JOINT_LEFT_HIP].real.z;
    myjoints[count++] = joints[JOINT_LEFT_HIP].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_HIP].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_HIP].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_HIP].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_HIP].confidence;

    myjoints[count++] = joints[JOINT_LEFT_ELBOW].real.x;
    myjoints[count++] = joints[JOINT_LEFT_ELBOW].real.y;
    myjoints[count++] = joints[JOINT_LEFT_ELBOW].real.z;
    myjoints[count++] = joints[JOINT_LEFT_ELBOW].confidence;

    myjoints[count++] = joints[JOINT_LEFT_WRIST].real.x;
    myjoints[count++] = joints[JOINT_LEFT_WRIST].real.y;
    myjoints[count++] = joints[JOINT_LEFT_WRIST].real.z;
    myjoints[count++] = joints[JOINT_LEFT_WRIST].confidence;

    myjoints[count++] = joints[JOINT_LEFT_HAND].real.x;
    myjoints[count++] = joints[JOINT_LEFT_HAND].real.y;
    myjoints[count++] = joints[JOINT_LEFT_HAND].real.z;
    myjoints[count++] = joints[JOINT_LEFT_HAND].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_ELBOW].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_ELBOW].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_ELBOW].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_ELBOW].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_WRIST].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_WRIST].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_WRIST].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_WRIST].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_HAND].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_HAND].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_HAND].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_HAND].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_KNEE].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_KNEE].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_KNEE].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_KNEE].confidence;

    myjoints[count++] = joints[JOINT_LEFT_KNEE].real.x;
    myjoints[count++] = joints[JOINT_LEFT_KNEE].real.y;
    myjoints[count++] = joints[JOINT_LEFT_KNEE].real.z;
    myjoints[count++] = joints[JOINT_LEFT_KNEE].confidence;

    myjoints[count++] = joints[JOINT_RIGHT_ANKLE].real.x;
    myjoints[count++] = joints[JOINT_RIGHT_ANKLE].real.y;
    myjoints[count++] = joints[JOINT_RIGHT_ANKLE].real.z;
    myjoints[count++] = joints[JOINT_RIGHT_ANKLE].confidence;

    myjoints[count++] = joints[JOINT_LEFT_ANKLE].real.x;
    myjoints[count++] = joints[JOINT_LEFT_ANKLE].real.y;
    myjoints[count++] = joints[JOINT_LEFT_ANKLE].real.z;
    myjoints[count++] = joints[JOINT_LEFT_ANKLE].confidence;


    env->SetDoubleArrayRegion(jarr1, 0, 114, myjoints);

    /*for (int i = 0; i < count; i++) {
        char out[10];
        snprintf(out, 10, "%d) %lf", i, myjoints[i]);
        __android_log_write(ANDROID_LOG_INFO, "Radu", out);
    }*/

    jclass clazz = env->FindClass("com/mygdx/game/AndroidLauncher");
    if (clazz == NULL) {
         __android_log_write(ANDROID_LOG_INFO, "Radu", "clazz null");
         return;
    }

    jmethodID mid = env->GetStaticMethodID(clazz, "logJoints", "([D)V");
    if (mid == NULL) {
         __android_log_write(ANDROID_LOG_INFO, "Radu", "method ID null");
         return;
    }

    env->CallStaticVoidMethod(clazz, mid, jarr1);

    env->ReleaseDoubleArrayElements(jarr1, myjoints, JNI_ABORT);
    env->DeleteLocalRef(jarr1);
    //env->DeleteLocalRef(myjoints);
    env->DeleteLocalRef(clazz);
}
