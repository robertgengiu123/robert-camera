package com.mygdx.game;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygdx.game.MyGdxGame;
import com.tdv.nuitrack.sdk.Nuitrack;

public class AndroidLauncher extends AndroidApplication {

	static {
		System.loadLibrary("native-lib");
	}

	static AndroidLauncher obj;
	MyGdxGame game;
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		obj = this;

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		game = new MyGdxGame();


		SharedPreferences sharedPref = getSharedPreferences("tracking_state", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("state", false);
		editor.commit();


		Nuitrack.init(this, new Nuitrack.NuitrackCallback() {
			@Override
			public void onInitSuccess(Context context) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						startTracking();
						Log.v("Radu", "Thread started");
					}
				}).start();
			}

			@Override
			public void onInitFailure(int i) {

			}
		});

		initialize(game, config);

	}

	public native String stringFromJNI();
	public native String startTracking();
	public native String stopTracking();


	public static void logJoints(double[] xy) {
		if (MyGdxGame.closed)
		{
			obj.stopTracking();
			return;
		}
		Log.d("Radu", "Log joints in Android " + xy.length);
//		for (int i = 0; i < xy.length; i+=2) {
//			Log.d("Radu", xy[i] + " " + xy[i+1]);
//		}

		MyGdxGame.receiveJoints(xy);
	}
}
